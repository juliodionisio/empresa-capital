package br.com.itau.capital.consumer;


import br.com.itau.capital.producer.Capital;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cnpj", url = "https://receitaws.com.br/")
public interface CnpjClient {

    @GetMapping("v1/cnpj/{cnpj}")
    Capital getByCnpj(@PathVariable String cnpj);
}
