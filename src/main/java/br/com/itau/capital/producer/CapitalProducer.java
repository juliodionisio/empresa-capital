package br.com.itau.capital.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CapitalProducer {
    @Autowired
    KafkaTemplate<String, Capital> producer;

    public void enviarAoKafka(Capital capital) {
        producer.send("spec3-julio-felipe-3", capital);
    }
}
