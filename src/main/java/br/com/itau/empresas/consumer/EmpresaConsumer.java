package br.com.itau.empresas.consumer;

import br.com.itau.capital.producer.Capital;
import br.com.itau.capital.consumer.CnpjClient;
import br.com.itau.capital.producer.CapitalProducer;
import br.com.itau.empresas.producer.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    CnpjClient cnpjClient;

    @Autowired
    CapitalProducer capitalProducer;

    @KafkaListener(topics = "spec3-julio-felipe-2", groupId = "gazorpazorp")
    public void receber(@Payload Empresa empresa){
        System.out.println("Recebi a empresa " + empresa.getNome() + ", com CNPJ " + empresa.getCnpj());

        Capital capital = new Capital();
        capital.setCapital_social(cnpjClient.getByCnpj(empresa.getCnpj()).getCapital_social());
        capital.setNome(cnpjClient.getByCnpj(empresa.getCnpj()).getNome());
        capital.setCnpj(cnpjClient.getByCnpj(empresa.getCnpj()).getCnpj());


        System.out.println(capital.getCapital_social());
       if(capital.getCapital_social()<1000000){
            System.out.println("Empresa recusada");
        } else {
           System.out.println("Empresa aceita");
           capitalProducer.enviarAoKafka(capital);
       }


    }
}
